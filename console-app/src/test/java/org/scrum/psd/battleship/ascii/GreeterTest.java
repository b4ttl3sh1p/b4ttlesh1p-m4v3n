package org.scrum.psd.battleship.ascii;

//import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

//import org.junit.Assert;
import org.junit.Test;

public class GreeterTest {
    private Greeter greeter = new Greeter();

    @Test
    public void greeterSaysHello() {
      //assertTrue(greeter.sayHello().contains("Hello"));
      assertEquals("Hello", greeter.sayHello());
    }
}
